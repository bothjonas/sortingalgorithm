
public class Insertion implements Algorithm{
    public static void doSort(int arr_rdmNumbers[]) {  
        int n = arr_rdmNumbers.length;  
        for (int j = 1; j < n; j++) {  
            int key = arr_rdmNumbers[j];  
            int i = j-1;  
            while ( (i > -1) && ( arr_rdmNumbers [i] > key ) ) {  
            	arr_rdmNumbers [i+1] = arr_rdmNumbers [i];  
                i--;  
            }  
            arr_rdmNumbers[i+1] = key;  
        }  
    }  
       
}
