
public class Selection implements Algorithm{
	 public static void selectionSort(int[] arr_rdmNumbers){  
	        for (int i = 0; i < arr_rdmNumbers.length - 1; i++)  
	        {  
	            int index = i;  
	            for (int j = i + 1; j < arr_rdmNumbers.length; j++){  
	                if (arr_rdmNumbers[j] < arr_rdmNumbers[index]){  
	                    index = j;//searching for lowest index  
	                }  
	            }  
	            int smallerNumber = arr_rdmNumbers[index];   
	            arr_rdmNumbers[index] = arr_rdmNumbers[i];  
	            arr_rdmNumbers[i] = smallerNumber;  
	        }  
	 }
}