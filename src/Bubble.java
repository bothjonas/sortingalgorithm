
public class Bubble implements Algorithm{
	 public static void doSort(int arr_rdmNumbers[]) {

	        int inputLength = arr_rdmNumbers.length;
	        int temp;
	        boolean is_sorted;

	        for (int i = 0; i < inputLength; i++) {

	            is_sorted = true;

	            for (int j = 1; j < (inputLength - i); j++) {

	                if (arr_rdmNumbers[j - 1] > arr_rdmNumbers[j]) {
	                    temp = arr_rdmNumbers[j - 1];
	                    arr_rdmNumbers[j - 1] = arr_rdmNumbers[j];
	                    arr_rdmNumbers[j] = temp;
	                    is_sorted = false;
	                }

	            }

	            // is sorted? then break it, avoid useless loop.
	            if (is_sorted) break;

	            System.out.println("\n");
	            
	        }
	        
	    }
}
